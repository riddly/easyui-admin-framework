﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using App.Core.Domain;
using App.Core.Domain.BaseObject;
using App.Core.Dtos;

namespace App.Services
{
    public partial interface IOrderService
    {
				/// <summary>
        /// 创建
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> CreateOrder(OrderDto dto,Guid? userId = null);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> UpdateOrder(Guid id, OrderDto dto);

        /// <summary>
        /// 异步获取
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Order> GetOrderAsync(Guid id);

        /// <summary>
        /// 异步获取
        /// </summary>
        /// <param name="orderSN"></param>
        /// <returns></returns>
        Task<Order> GetOrderAsync(string orderSN);

        /// <summary>
        /// 异步获取
        /// </summary>
        /// <param name="pression"></param>
        /// <returns></returns>
        Task<AddressDto> GetLastAddressAsync(Guid? userId);

        /// <summary>
        /// 修改订单状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> SetOrderStatus(Guid id, OrderStatusDto dto);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="idList"></param>
        /// <returns></returns>
        Task DeleteAsync(Guid[] idList);
    }
}
