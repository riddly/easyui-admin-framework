﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using App.Core.Domain;
using App.Core.Domain.BaseObject;
using App.Core.Dtos;

namespace App.Services
{
    public partial interface IAddressService
    {
				/// <summary>
        /// 创建
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> CreateAddress(AddressDto dto);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<OperationResult> UpdateAddress(Guid id, AddressDto dto);
		
		/// <summary>
        /// 异步获取
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Address> GetAddressAsync(Guid id);

        Task<Address> GetAddressAsync(Expression<Func<Address, bool>> pression);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<OperationResult> DeleteAsync(Guid id);

        Task<List<AddressDto>> GetUserAddress(Guid userId);
    }
}
