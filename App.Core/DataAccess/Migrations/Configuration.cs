using App.Core.Common;
using App.Core.Domain;
using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace App.Core.DataAccess.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Db>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Db context)
        {
            //context.User.AddOrUpdate(
            //  new User
            //  {
            //      UserName = "admin",
            //      PasswordHash = PasswordHash.CreateHash("123456"),
            //      Team = new Team { TeamName = "系统管理员" },
            //      Roles = new List<Role> { 
            //          new Role
            //          {
            //              RoleName = "Administrator",
            //              Menus = new List<Menu> { 
            //                new Menu{ MenuText="起始页", MenuLink="/UCenter", SortOrder=1}
            //              }
            //          }
            //      }
            //  }
            //);
        }
    }
}
