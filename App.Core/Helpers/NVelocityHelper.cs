﻿using App.Core.Common;
using Commons.Collections;
using NVelocity;
using NVelocity.App;
using NVelocity.Runtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Helpers
{
    public class NVelocityHelper
    {
        private static volatile NVelocityHelper instance = null;
        private static object lockHelper = new object();

        public static NVelocityHelper GetInstance()
        {
            if (instance == null)
            {
                lock (lockHelper)
                {
                    if (instance == null)
                    {
                        instance = new NVelocityHelper();
                    }
                }
            }

            return instance;
        }

        /// <summary>
        /// VelocityHelper 的构造函数
        /// </summary>
        public NVelocityHelper()
        {

        }

        /// <summary>
        /// 获取解析后的模板内容
        /// </summary>
        /// <param name="templateName"></param>
        /// <param name="tlpPath"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public StringBuilder GetTemplate(string templateName, string tlpPath, IDictionary context)
        {
            return this.GetTemplate(templateName, tlpPath, context, true);
        }

        /// <summary>
        /// 获取解析后的String类模板内容
        /// </summary>
        /// <param name="template"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public StringBuilder GetTemplate(string template, IDictionary context)
        {
            return this.GetTemplate(template, null, context, false);
        }

        /// <summary>
        /// 获取解析后的模板内容
        /// </summary>
        /// <param name="template"></param>
        /// <param name="tlpPath"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private StringBuilder GetTemplate(string template, string tlpPath, IDictionary context, bool isFile = false)
        {
            VelocityEngine engine = new VelocityEngine();

            VelocityContext vc = new VelocityContext();
            foreach (DictionaryEntry de in context)
            {
                vc.Put(de.Key.ToString(), de.Value);
            }

            if (isFile)
            {
                ExtendedProperties prop = new ExtendedProperties();
                prop.AddProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, Utils.GetMapPath(tlpPath));
                prop.AddProperty(RuntimeConstants.ENCODING_DEFAULT, "utf-8");
                prop.AddProperty(RuntimeConstants.FILE_RESOURCE_LOADER_CACHE, true); //是否启用模板缓存

                engine.Init(prop);

                Template tlp = engine.GetTemplate(template + ".tlp");

                using (var sw = new StringWriter())
                {
                    tlp.Merge(vc, sw);

                    return sw.GetStringBuilder();
                }
            }
            else
            {
                engine.Init();

                using (var sw = new StringWriter())
                {
                    engine.Evaluate(vc, sw, "", template);
                    return sw.GetStringBuilder();
                }
            }
        }
    }
}
