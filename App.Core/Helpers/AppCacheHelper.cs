﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using App.Core.DataAccess;
using App.Core.Domain;
using App.Core.Domain.BaseObject;

namespace App.Core.Helpers
{
    public static class AppCacheHelper
    {
        /// <summary>
        /// 获取配置信息
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GetSettings()
        {
            var settings = WebCache.Get("app.setting") as Dictionary<string, string>;
            if (settings != null) return settings;

            using (var db = new Db())
            {
                var data = db.Setting.Select(s => new { s.Key, s.Value }).OrderBy(s => s.Key).ToDictionary(s => s.Key, s => s.Value);

                WebCache.Set("app.setting", data);

                return data;
            }
        }

        /// <summary>
        /// 获取指定键的配置信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetSetting(string key) {
            return GetSettings().Where(s => s.Key == key).Select(s => s.Value).SingleOrDefault();
        }
    }
}
 