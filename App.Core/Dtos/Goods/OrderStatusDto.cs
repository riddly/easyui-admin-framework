﻿namespace App.Core.Dtos
{
    public class OrderStatusDto
    {
        public int OrderStatus { get; set; }

        public string TrackingNumber { get; set; }
    }
}
