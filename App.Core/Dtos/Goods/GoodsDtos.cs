﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Dtos
{
    public partial class BrandDto : BulkEntityDto
    {
        [Required]
        public string BrandName { get; set; }
    }

    public partial class GoodsCategoryDto {
        [Required]
        public string CategoryName { get; set; }
        public Guid? ParentId { get; set; }
    }

    public partial class GoodsDto
    {
        public Guid? BrandId { get; set; }
        public List<Guid> GoodsCategorys { get; set; }
        [Required]
        public string GoodsName { get; set; }
        public string Photo { get; set; }
        public decimal Price { get; set; }
        public string Content { get; set; }
    }
}
