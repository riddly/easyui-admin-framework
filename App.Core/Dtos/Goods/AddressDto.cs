﻿namespace App.Core.Dtos
{
    public class AddressDto
    {
        /// <summary>
        /// 收件人
        /// </summary>
        public string Consignee { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string ConsigneePhone { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public string ConsigneeSex { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string ConsigneeEmail { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        public string ConsigneeProvince { get; set; }
        /// <summary>
        /// 城市 
        /// </summary>
        public string ConsigneeCity { get; set; }
        /// <summary>
        /// 区/县
        /// </summary>
        public string ConsigneeDistrict { get; set; }
        /// <summary>
        /// 收货地址
        /// </summary>
        public string ConsigneeAddress { get; set; }
    }
}
