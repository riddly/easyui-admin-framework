﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Core.Dtos
{
    public partial class UserDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfrimPassword { get; set; }
        public string Email { get; set; }
        [MaxLength(11)]
        public string Mobile { get; set; }
        public List<Guid> RoleId { get; set; }
        public Guid TeamId { get; set; }
        public string OpenId { get; set; }
    }

    public partial class TeamDto
    {
        public Guid? ParentId { get; set; }
        [Required]
        public string TeamName { get; set; }
        public string TeamDesc { get; set; }
        public int SortOrder { get; set; }
    }

    public partial class ChangePwdDto
    {
        public string OldPassword { get; set; }
        public string Password { get; set; }
    }
}
