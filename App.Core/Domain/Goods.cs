﻿using App.Core.Domain.BaseObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Domain
{
    /// <summary>
    /// 商品品牌
    /// </summary>
    public class GoodsBrand : FullAuditedEntity
    {
        [StringLength(ColumnSetting.Short)]
        public string BrandName { get; set; }
        public Seo Seo { get; set; }
        public ICollection<Goods> Goods { get; set; }
    }

    /// <summary>
    /// 商品分类
    /// </summary>
    public class GoodsCategory : FullAuditedEntity
    {
        [StringLength(ColumnSetting.Short)]
        public string CategoryName { get; set; }
        public Guid? ParentId { get; set; }
        public Seo Seo { get; set; }
        [ForeignKey("ParentId")]
        public virtual ICollection<GoodsCategory> ChildCategorys { get; set; }
        public ICollection<Goods> Goods { get; set; }
    }

    /// <summary>
    /// 商品
    /// </summary>
    public class Goods : FullAuditedEntity
    {
        [StringLength(ColumnSetting.Longer)]
        public string GoodsName { get; set; }
        public decimal Price { get; set; }
        [StringLength(ColumnSetting.Url)]
        public string Photo { get; set; }
        public string Content { get; set; }
        public Seo Seo { get; set; }
        public GoodsBrand Brand { get; set; }
        public ICollection<GoodsCategory> Categorys { get; set; }
        public ICollection<GoodsPackage> GoodsPackages { get; set; }
    }

    /// <summary>
    /// 商品套餐
    /// </summary>
    public class GoodsPackage : FullAuditedEntity
    {
        [StringLength(ColumnSetting.Longer)]
        public string PackageName { get; set; }
        [StringLength(ColumnSetting.Url)]
        public string Photo { get; set; }
        public decimal Price { get; set; }
        /// <summary>
        /// 兑换码数量
        /// </summary>
        public int TotalRedeemCode { get; set; }
        [StringLength(3)]
        public string CustomRedeemCode { get; set; }
        public string Description { get; set; }
        public ICollection<Goods> Goods { get; set; }
        public ICollection<RedeemCode> RedeemCodes { get; set; }
    }

    /// <summary>
    /// 兑换码
    /// </summary>
    public class RedeemCode : FullAuditedEntity
    {
        public GoodsPackage Package { get; set; }
        [StringLength(15)]
        public string CodeNumber { get; set; }
        public bool IsExchange { get; set; }
        public DateTime? ExchangeTime { get; set; }
        public Guid? ExchangeUserId { get; set; }
        public Guid? ExchangeOrderId { get; set; }
    }

    /// <summary>
    /// 订单
    /// </summary>
    public class Order : FullAuditedEntity,IDeletionAudited
    {
        /// <summary>
        /// 订单号
        /// </summary>
        [StringLength(ColumnSetting.VeryShort)]
        public string OrderSN { get; set; }
        /// <summary>
        /// 订单金额
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 订货人手机号
        /// </summary>
        [StringLength(ColumnSetting.Phone)]
        public string Mobile { get; set; }
        /// <summary>
        /// 兑换码
        /// </summary>
        [StringLength(ColumnSetting.Short)]
        public string RedeemCodes { get; set; }
        /// <summary>
        /// 收件人
        /// </summary>
        [StringLength(ColumnSetting.VeryShort)]
        public string Consignee { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [StringLength(ColumnSetting.Phone)]
        public string ConsigneePhone { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        [StringLength(ColumnSetting.SmallShort)]
        public string ConsigneeSex { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [StringLength(ColumnSetting.Short)]
        public string ConsigneeEmail { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        [StringLength(ColumnSetting.SmallShort)]
        public string ConsigneeProvince { get; set; }
        /// <summary>
        /// 城市 
        /// </summary>
        [StringLength(ColumnSetting.SmallShort)]
        public string ConsigneeCity { get; set; }
        /// <summary>
        /// 区/县
        /// </summary>
        [StringLength(ColumnSetting.SmallShort)]
        public string ConsigneeDistrict { get; set; }
        /// <summary>
        /// 收货地址
        /// </summary>
        [StringLength(ColumnSetting.Long)]
        public string ConsigneeAddress { get; set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        public int OrderStatus { get; set; }

        [StringLength(ColumnSetting.VeryShort)]
        public string TrackingNumber { get; set; }

        public bool IsDeleted { get; set; }

        public Guid? DeleterUserId { get; set; }

        public DateTime? DeletionTime { get; set; }
    }

    public class Address : FullAuditedEntity {
        /// <summary>
        /// 收件人
        /// </summary>
        [StringLength(ColumnSetting.VeryShort)]
        public string Consignee { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [StringLength(ColumnSetting.Phone)]
        public string ConsigneePhone { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        [StringLength(ColumnSetting.SmallShort)]
        public string ConsigneeSex { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [StringLength(ColumnSetting.Short)]
        public string ConsigneeEmail { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        [StringLength(ColumnSetting.SmallShort)]
        public string ConsigneeProvince { get; set; }
        /// <summary>
        /// 城市 
        /// </summary>
        [StringLength(ColumnSetting.SmallShort)]
        public string ConsigneeCity { get; set; }
        /// <summary>
        /// 区/县
        /// </summary>
        [StringLength(ColumnSetting.SmallShort)]
        public string ConsigneeDistrict { get; set; }
        /// <summary>
        /// 收货地址
        /// </summary>
        [StringLength(ColumnSetting.Long)]
        public string ConsigneeAddress { get; set; }
    }
}
