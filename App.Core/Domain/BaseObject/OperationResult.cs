﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Domain.BaseObject
{
    /// <summary>
    /// 操作结果
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class OperationResult<T>
    {
        public OperationResult()
        {
            message = "操作成功。";
        }

        public bool success { get; set; }
        public string message { get; set; }
        public T model { get; set; }
    }

    /// <summary>
    /// 操作结果
    /// </summary>
    public class OperationResult
    {
        public OperationResult()
        {
            success = true;
            message = "操作成功。";
        }

        public bool success { get; set; }
        public string message { get; set; }
    }
}
