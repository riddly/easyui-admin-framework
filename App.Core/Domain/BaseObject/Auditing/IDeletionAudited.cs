﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Domain.BaseObject
{
    public interface IDeletionAudited : ISoftDelete
    {
        Guid? DeleterUserId { get; set; }
        DateTime? DeletionTime { get; set; }
    }
}
