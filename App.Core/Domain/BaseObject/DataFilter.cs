﻿namespace App.Core.Domain.BaseObject
{
    public class DataFilter
    {
        /// <summary>
        /// 类型
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// 字段
        /// </summary>
        public string field { get; set; }
        /// <summary>
        /// comparison
        /// </summary>
        public string op { get; set; }
    }
}
