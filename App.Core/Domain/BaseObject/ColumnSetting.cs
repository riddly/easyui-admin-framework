﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Domain.BaseObject
{
    public static class ColumnSetting
    {
        public const int Short = 50;
        public const int Longer = 200;
        public const int Long = 100;
        public const int Remark = 300;
        public const int Url = 300;
        public const int VeryShort = 20;
        public const int SmallShort = 10;
        public const int Phone = 15;
    }
}
