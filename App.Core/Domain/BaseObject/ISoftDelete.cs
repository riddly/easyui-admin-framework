﻿namespace App.Core.Domain.BaseObject
{
    public interface ISoftDelete
    {
        bool IsDeleted { get; set; }
    }
}
