﻿using System.Collections.Generic;

namespace App.Core.Domain.BaseObject
{
    /// <summary>
    /// 结果列表
    /// </summary>
    public class GridResult
    {
        /// <summary>
        /// 列表记录总数
        /// </summary>
        public long total { get; set; }
        /// <summary>
        /// 列表记录集合【object】
        /// </summary>
        public object rows { get; set; }
    }

    public class GridResult<T>
    {
        public long total { get; set; }
        public List<T> rows { get; set; }
    }
}
